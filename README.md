About
=========
----------
Hi!

**Project**: Tribe app for notion integration
**Description**: It will integrate Notion knowledge base into the Tribe community 

Please refer to [this notion public page](https://harzaanapp.notion.site/Tribe-job-b7e7a44a8946481195935fc7976d5e3b) for the full documentation.
