
const notionProperties = [
  {
    name: 'name',
    type: 'name',
    value: '',
    path: 'post.title'
  },
  {
    name: 'Status',
    type: 'select',
    value: 'unverified',
    path: '',
  },
  {
    name: 'Author',
    type: 'text',
    value: '',
    path: 'post.ownerId',
  },
  {
    name: 'Space',
    type: 'text',
    value: '',
    path: 'post.spaceId',
  },
  {
    name: 'PostId',
    type: 'text',
    value: '',
    path: 'post.id',
  },
];

const initialCurryObj = {
  notionId: '',
  tag: '',
  notionObj: {
    headers: {
      'Content-Type': '',
      'Notion-Version': '',
      'Authorization': ''
    },
    body: {
      parent: {
        database_id: 'string',
      },
      properties: []
    }
  },
  post: {}
};

export {
  notionProperties,
  initialCurryObj
}