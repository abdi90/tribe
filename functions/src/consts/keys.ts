import serviceAccount from "./serviceAccount.json";
const firebase = {               //clone json object into new object to make typescript happy
    type: serviceAccount.type,
    projectId: serviceAccount.project_id,
    privateKeyId: serviceAccount.private_key_id,
    privateKey: serviceAccount.private_key,
    clientEmail: serviceAccount.client_email,
    clientId: serviceAccount.client_id,
    authUri: serviceAccount.auth_uri,
    tokenUri: serviceAccount.token_uri,
    authProviderX509CertUrl: serviceAccount.auth_provider_x509_cert_url,
    clientC509CertUrl: serviceAccount.client_x509_cert_url
}

const firebaseApp = {
  apiKey: "AIzaSyDEd7vZ4eEG_WJaXenVQlurBakS7X6OSsM",
  authDomain: "tribe-abdi.firebaseapp.com",
  projectId: "tribe-abdi",
  storageBucket: "tribe-abdi.appspot.com",
  messagingSenderId: "297440137333",
  appId: "1:297440137333:web:06c03bf52b1ab998a4283c"
}

const tribe = {
  graphqlUrl: 'https://app.tribe.so/graphql',
  clientId: "3c4d8598-c31cc6336dea",
  clientSecret: "d28316342b524f4fa8c236dd2708213c",
};

const notion = {
  createLink: "https://api.notion.com/v1/pages",
  authorization: "secret_4gcuFdQ6rc5nglVr4S2E89qm600ctFkDtiIttsLgFV3",
  database: "0238f0d423b344c3b9b781b8bb50c50c",
};

export {
  tribe,
  notion,
  firebase,
  firebaseApp
};