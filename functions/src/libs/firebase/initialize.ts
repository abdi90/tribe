
import firebase from "firebase";
import { firebaseApp } from "../../consts";

const firebaseInitialize = () => {
  firebase.initializeApp(firebaseApp);
  firebase.functions().useEmulator("localhost", 5001);
}

export {
  firebaseInitialize
}

