type notionType = {
  headers: {
    'Content-Type': string,
    'Notion-Version': string,
    'Authorization': string
  },
  body: {
    parent: {
      database_id: string,
    },
    properties: Array<property>
  }
}

type property = {
  name: string,
  value: string | number | undefined,
  type: string,
  path: string,
  maker: any
}

type curryObj = {
  notionId: string,
  tag: string,
  notionObj: notionType,
  post: data | {}
};

type objectType = {
  id: string,
  networkId: string,
  spaceId: string,
  createdAt: string,
  updatedAt: string,
  title: string,
  slug: string,
  description: string | null
};

type target = {
  organizationId: string,
  networkId: string,
  collectionId: string,
  spaceId: string,
  postId: string,
  memberId: string,
  memberRoleId: string
};

type actor = {
  id: string,
  roleId: string,
  roleType: string,
  sessionInfo: Array<any>,
  spaceRoleId: string,
  spaceRoleType: string
};

type data = {
  time: string,
  verb: string,
  verbAction: string,
  actor: actor,
  object: objectType,
  target: target,
  id: string,
  name: string,
  noun: string,
  shortDescription: string
}

type webhook = {
  networkId: string,
  context: string,
  entityId: string,
  currentSettings: Array<any>,
  type: string,
  data: data,

}

type getPostId_t = (obj: curryObj) => string | undefined;
type getTag_t = (post: data) => string | undefined;
type getBodyProperties_t =  (obj: curryObj) => Array<property> | undefined;
type addParent_t = (obj: curryObj) => curryObj;
type addHeader_t = (obj: curryObj) => curryObj;
type mapTribeToNotion_t = (obj: curryObj) => curryObj;
type setPost_t = (obj: curryObj) => void;
type findNotionId_t = (data: data) => string;
type buildNotionObj_t = (obj: curryObj) => curryObj;
type fetchPost_t = (id: string) => any

export {
  notionType,
  property,
  curryObj,
  webhook,
  data,
  getPostId_t,
  getTag_t,
  getBodyProperties_t,
  addParent_t,
  addHeader_t,
  mapTribeToNotion_t,
  setPost_t,
  findNotionId_t,
  buildNotionObj_t,
  fetchPost_t
}