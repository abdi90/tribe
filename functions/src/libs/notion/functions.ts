// vendor
import { 
  cond, 
  propEq,
  assocPath,
  compose,
  path,
  slice,
  split,
  map,
  __,
  assoc,
  defaultTo,
  tap,
} from "ramda"

// local
import type {
  curryObj, 
  notionType, 
  property, 
  getTag_t, 
  getBodyProperties_t, 
  addParent_t, 
  addHeader_t, 
  mapTribeToNotion_t,
  getPostId_t
} from './types';
import { notion } from "../../consts/keys";
import { notionProperties } from "../../consts/constants";


const nameMaker = (item: property) => ({
  Name:{
    title: [
      {
        text: {
          content: item.value
        }
      }
    ]
  } 
})

const urlMaker = (item: property) => ({
  [item.name]:{
    "url": item.value
  } 
})

const phoneMaker = (item: property) => ({
  [item.name]:{
    "phone_number": item.value
  } 
});

const selectMaker = (item: property) => ({
  [item.name]:{
    "select": {
      "name": item.value
    }
  } 
});

const numberMaker = (item: property) => ({
  [item.name]:{
    "number": item.value
  } 
});

const stringMaker = (item: property) => ({
  [item.name]:{
    "phone_number": item.value
  } 
});

const textMaker = (item: property) => ({
  [item.name]: {
    rich_text: [
      {
        text: {
          content: item.value,
        },
      },
    ],
  },
});

// It needs time to figure out what's the notion algorithm to set the contents
const contentMaker = cond([]);


// how to fin
const notionPoropertyMaker:(item: property) => any = cond([
  [propEq('type', 'name'), nameMaker],
  [propEq('type', 'url'), urlMaker],
  [propEq('type', 'select'), selectMaker],
  [propEq('type', 'number'), numberMaker],
  [propEq('type', 'string'), stringMaker],
  [propEq('type', 'phoneNumber'), phoneMaker],
  [propEq('type', 'text'), textMaker],

]);

const mapProperties = (obj: curryObj, properties: Array<property>) => map(item => {
  if (!item.value) {
    item.value = path(split('.', item.path), obj)
  };
  const mapped = notionPoropertyMaker(item);
  return mapped;
}, properties);

const addProperties = (obj: curryObj) => compose(
  assocPath(["notionObj", "body", "properties"], __, obj),
  (properties) => Object.assign({}, ...properties),
  (properties: Array<property>) => mapProperties(obj, properties),
  defaultTo([]),
  getBodyProperties
)(obj);

const addHeader: addHeader_t = assocPath(["notionObj", "headers"], {
  "Content-Type": "application/json",
  "Notion-Version": "2021-08-16",
  "Authorization": notion.authorization,
});

const addParent: addParent_t = assocPath(["notionObj", "body", "parent", "database_id"], notion.database)

const getBodyProperties: getBodyProperties_t = path(["notionObj", "body", "properties"])
const addPropertiesSchema = assocPath(["notionObj", "body", "properties"], notionProperties);

// mapping tribe blocks to notion blocks needs significant time
// because we should list all the tribe blocks with the structure details and map them to notion 
// so I will leave it for now
const mapTribeToNotion: mapTribeToNotion_t = (curryObj) => curryObj;


// TODO: temporary add undefined type
const getTag: getTag_t = path(["object", "title"])

const getPostId: getPostId_t = path(['post', 'target', 'postId']);

// we will hardcode this until we find a way to get database by name
// Its notion limitation
const findDatabaseId = () => notion.database;
const setNotionId = (id: string, obj: notionType) => assoc("notionId", id, obj);
const dashSplit: (item: string) => Array<string> = split("-");
const skipFirstCell = slice(1, Infinity);

const sayX = (x: any) => console.log('x is ' + x);
const log = tap(sayX);
const defaultStr = defaultTo("");

export {
  notionPoropertyMaker,
  contentMaker,
  addHeader,
  addParent,
  getBodyProperties,
  addPropertiesSchema,
  addProperties,
  mapTribeToNotion,
  getTag,
  findDatabaseId,
  dashSplit,
  setNotionId,
  skipFirstCell,
  log,
  getPostId,
  defaultStr,
  nameMaker,
  urlMaker,
  phoneMaker,
  selectMaker,
  numberMaker,
  stringMaker,
  textMaker
}