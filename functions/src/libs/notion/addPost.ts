// vendor
import axios from "axios";
import {
  andThen,
  assocPath,
  compose,
  composeWith,
} from "ramda";
import { 
  addHeader,
  addParent,
  addProperties,
  mapTribeToNotion,
  getTag,
  findDatabaseId,
  dashSplit,
  skipFirstCell,
  defaultStr,
 } from "./functions";

// locals
import type {
  buildNotionObj_t,
  curryObj, 
  data,
  findNotionId_t,
  setPost_t, 
} from "./types";
import { initialCurryObj, notion, notionProperties } from "../../consts";
import { getPost } from "../tribe";

const buildNotionObj: buildNotionObj_t = compose (
  mapTribeToNotion,
  addProperties,
  addParent,
  addHeader
)

const findNotionId: findNotionId_t = compose(
  findDatabaseId,
  skipFirstCell,
  dashSplit,
  defaultStr,
  getTag
)

const makeRequest = (obj: curryObj) : any => {
  const {body, headers} = obj.notionObj;
  console.log("🚀 ~ file: addPost.ts ~ line 50 ~ body, headers", body, headers)
  return axios.post( notion.createLink,  body, {headers})
    .then(() => console.log("success"))
    .catch(err => console.error(err));
}

const setPost: setPost_t = composeWith(andThen)([
  makeRequest,
  buildNotionObj,
  getPost,
]);

// Main component
const addPost = (data: data) => {
  // I use curryObj, to take it through compose flow And set the properties one by one
  const curryObj: curryObj =  initialCurryObj;

  compose(
    setPost,
    assocPath(["post"], data),
    assocPath(["notionId"], findNotionId(data)),
    assocPath(["notionObj", "body",  "properties"], notionProperties)
  )(curryObj)

};

export {
  addPost
}