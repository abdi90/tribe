import { 
  andThen,
  compose,
  assoc,
  __,
  composeWith,
 } from 'ramda';

// locals
import { defaultStr, getPostId } from '../notion/functions';
import { curryObj } from '../notion/types';
import { client, getToken } from './initialize';

const setToken = (guestTokens: any) => client.setToken(guestTokens.accessToken);

const makeRequest = async(id: string): Promise<any> => client.posts.get(id);

const fetchPost = async (id: string): Promise<any> => composeWith(andThen)([
  () => makeRequest(id),
  setToken,
  getToken,
])(id);

const getId: (obj: curryObj) => string = compose (
  defaultStr,
  getPostId
)

const getPost = async(obj: curryObj): Promise<curryObj> => compose(
  andThen((post: any) => assoc('post', post, obj)),
  fetchPost,
  () => getId(obj)
)()

export {
  fetchPost,
  getPost
}
