// vendor
import { TribeClient } from '@tribeplatform/gql-client';
import {tribe} from '../../consts/keys';

const client = new TribeClient({
  graphqlUrl: tribe.graphqlUrl,
});

const getToken = async () => {
  const guestTokens = await client.getTokens(
    { networkDomain: "harzaan.tribeplatform.com" },
    "basic"
  )
  // client.setToken(guestTokens.accessToken)
  console.log({ accessToken: guestTokens });

  return guestTokens;
}

export {
  client,
  getToken
}

