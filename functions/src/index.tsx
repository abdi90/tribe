// vendor
import * as functions from "firebase-functions";
import type { Request } from "firebase-functions/v1";
import { 
  cond,
  T, 
  propEq, 
  has, 
  allPass,
  when
} from "ramda";

// local
import { addPost, removePost } from "./libs/notion";
import { data } from "./libs/notion/types";
import { firebaseInitialize } from "./libs/firebase";

// initialize
firebaseInitialize();

exports.notionUpdater = functions.https.onRequest((req: Request): void | Promise<any> => {
    const { data } = req.body;
    // Functions
    const checkProps: (data: data) => boolean = allPass([
      has("verb"),
      propEq("noun", "TAG")
    ]);

    // Dispatch according to the verb property
    const dispatcher: (data: data) => void = cond([
      [propEq("verb", "ADDED"), addPost],
      [propEq("verb", "REMOVED"), removePost],
      [T, () => console.log('wrong')]
    ]);

    // Run it
    when( checkProps, dispatcher )(data);
  }
);
